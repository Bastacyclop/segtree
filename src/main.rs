use std::{io, str, fs, env, cmp};

struct Query {
    x: u64,
    w: u64,
    h: u64,
}

impl Query {
    fn from_line(line: &str) -> Self {
        let mut split = line.split_whitespace();
        Query {
            x: split.next().unwrap().parse().expect("cannot parse x"),
            w: split.next().unwrap().parse().expect("cannot parse w"),
            h: split.next().unwrap().parse().expect("cannot parse h"),
        }
    }
}

fn main() {
    use std::io::BufRead;

    let mut args = env::args().skip(1);
    let path = args.next().unwrap();
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    reader.read_line(&mut line).unwrap();
    let (n, w, h): (usize, u64, u64);
    {
        let mut split = line.split_whitespace();
        n = split.next().unwrap().parse().expect("cannot parse 'n'");
        w = split.next().unwrap().parse().expect("cannot parse 'r'");
        h = split.next().unwrap().parse().expect("cannot parse 't'");
    }
    println!("(n, w, h): {:?}", (n, w, h));
    line.clear();

    let mut queries = Vec::with_capacity(n);
    for _ in 0..n {
        reader.read_line(&mut line).unwrap();
        queries.push(Query::from_line(&line));
        line.clear();
    }

    queries.sort_by(|a, b| a.h.cmp(&b.h)); // better results

    let mut root = Node::leaf(1, w, h);
    let mut n = 0;
    for q in queries {
        if root.fits(&q) {
            root.insert(q.x, q.x + q.w, q.h);
            n += 1;
        }
    }

    println!("{}", n);
}

#[derive(Debug)]
struct Node {
    beg: u64,
    end: u64,
    remaining: u64,
    childs: Option<Box<[Node; 2]>>,
}

impl Node {
    fn leaf(beg: u64, end: u64, h: u64) -> Self {
        Node {
            beg: beg,
            end: end,
            remaining: h,
            childs: None
        }
    }

    fn fits(&self, query: &Query) -> bool {
        let (b, e) = (query.x, query.x + query.w);
        match self.childs {
            None => self.remaining >= query.h,
            Some(ref cs) => {
                let mid = cs[0].end;
                if e <= mid {
                    cs[0].fits(query)
                } else if b >= mid {
                    cs[1].fits(query)
                } else {
                    cs[0].fits(query) && cs[1].fits(query)
                }
            }
        }
    }

    fn insert(&mut self, b: u64, e: u64, h: u64) {
        debug_assert!(self.beg != self.end);
        match self.childs {
            None => {
                let r = self.remaining - h;
                if b == self.beg && e == self.end {
                    self.remaining = r;
                } else if b == self.beg {
                    self.childs = Some(Box::new([
                        Node::leaf(b, e, r),
                        Node::leaf(e, self.end, self.remaining),
                    ]));
                } else if e == self.end {
                    self.childs = Some(Box::new([
                        Node::leaf(self.beg, b, self.remaining),
                        Node::leaf(b, e, r),
                    ]));
                } else {
                    self.childs = Some(Box::new([
                        Node::leaf(self.beg, b, self.remaining),
                        Node {
                            beg: b,
                            end: self.end,
                            remaining: self.remaining,
                            childs: Some(Box::new([
                                Node::leaf(b, e, r),
                                Node::leaf(e, self.end, self.remaining),
                            ]))
                        }
                    ]));
                }
            },
            Some(ref mut cs) => {
                let mid = cs[0].end;
                if e <= mid {
                    cs[0].insert(b, e, h);
                } else if b >= mid {
                    cs[1].insert(b, e, h);
                } else {
                    cs[0].insert(b, mid, h);
                    cs[1].insert(mid, e, h);
                }
                self.remaining = cmp::max(cs[0].remaining, cs[1].remaining);
            }
        }
    }
}